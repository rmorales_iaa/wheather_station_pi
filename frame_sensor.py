# [sesors: temperature, humidity, pressure]
# BME280
# driver: https://pypi.org/project/RPi.bme280/
# datasheet: https://www.bosch-sensortec.com/bst/products/all_products/bme280
# --------------------------------------------------------------------------
import PySimpleGUI as sg
import bme280
from things_board import *
from global_var import *

# --------------------------------------------------------------------------
# All the stuff inside your window.
layout_sensors = [

[sg.Text('TEMPERATURE NOT INITIALIZED'
                               , key=TEXT_TEMPERATURE
                               , background_color='blue'
                               , text_color='yellow'
                               , font=('Helvetica', '70'))]
    ,

    [sg.Text('HUMIDITY NOT INITIALIZED'
                               , key=TEXT_HUMIDITY
                               , background_color='blue'
                               , text_color='#FF9D00'
                               , font=('Helvetica', '60'))]

    , [sg.Text('PRESSURE NOT INITIALIZED'
                               , key=TEXT_PRESSURE
                               , background_color='blue'
                               , text_color='#91f400'
                               , font=('Helvetica', '60'))]
]

# frame definition
frame_sensors= [sg.Frame(title=''
                          , layout=layout_sensors
                          , background_color='blue'
                          , element_justification='left')
                     ]
# --------------------------------------------------------------------------
def sensors_update(form):
    data = bme280.sample(BUS, BME280_ADDRESS)
    data_array = BME280_DATA_FORMAT.format(1, data.timestamp, data.temperature, data.pressure, data.humidity).split(',')

    last_value_temperature = data_array[1]
    last_value_pressure = data_array[2]
    last_value_humidity = data_array[3]

    form[TEXT_TEMPERATURE].Update(last_value_temperature)
    form[TEXT_PRESSURE].Update(last_value_pressure)
    form[TEXT_HUMIDITY].Update(last_value_humidity)

    things_board_update_data(THINGS_BOARD_DATA_INDEX_BME280_TEMPERATURE, last_value_temperature.replace(' deg C',''))
    things_board_update_data(THINGS_BOARD_DATA_INDEX_BME280_PRESSURE, last_value_pressure.replace(' hPa',''))
    things_board_update_data(THINGS_BOARD_DATA_INDEX_BME280_HUMIDITY, last_value_humidity.replace('% rel humidity',''))

# --------------------------------------------------------------------------
def sensors_init():
    bme280.load_calibration_params(BUS, BME280_ADDRESS)
# --------------------------------------------------------------------------
