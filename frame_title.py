# --------------------------------------------------------------------------
import PySimpleGUI as sg

from global_var import TEXT_CAPTION

# --------------------------------------------------------------------------

# frame layout
layout = [[sg.Text('maya weather station'
                   , key=TEXT_CAPTION
                   , background_color='blue'
                   , text_color='white'
                   , font=('Helvetica', '40')
                   )]]

# frame definition
frame_title = [sg.Frame(title=''
                        , layout=layout
                        , background_color='Black'
                        , element_justification='center'
                        )]
# --------------------------------------------------------------------------
