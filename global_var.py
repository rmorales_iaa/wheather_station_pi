# --------------------------------------------------------------------------
import gpsdshm
import pytz
import smbus2
# --------------------------------------------------------------------------
POLLING_TIME_MS = 1*1000
PUBLISHING_TIME_S = 60
# --------------------------------------------------------------------------
# bus
BUS = smbus2.SMBus(1)
# --------------------------------------------------------------------------
# BME280
BME280_ADDRESS = 0x76
BME280_DATA_FORMAT = '{1},{2:0.3f} deg C,{3:0.3f} hPa,{4:0.3f}% rel humidity'
# --------------------------------------------------------------------------
# gps
GPSD_SHM = gpsdshm.Shm()
# --------------------------------------------------------------------------
# things board
things_board_client = [0]
last_publishing_time_s = [0]

#connection
THINGS_BOARD_HOST = '161.111.164.212'
THINGS_BOARD_MQTT_PORT = 2883
THINGS_BOARD_CONNECTION_KEEP_ALIVE_INTERVAL_SECONDS = 60
THINGS_BOARD_TELEMETRY_TOPIC = 'v1/devices/me/telemetry'

#device
THINGS_BOARD_DEVICE_NAME = 'maya_weather_station_pi'
THINGS_BOARD_DEVICE_ACCESS_TOKEN = '5JZwl18zNjaISpLRc0zO'

# values of the device to publish
THINGS_BOARD_DATA_NAME_SEQ = [
'timestamp'
, 'bme280_temperature'
, 'bme280_pressure'
, 'bme280_humidity'
]

THINGS_BOARD_DATA_INDEX_TIME_STAMP = 0
THINGS_BOARD_DATA_INDEX_BME280_TEMPERATURE = 1
THINGS_BOARD_DATA_INDEX_BME280_PRESSURE = 2
THINGS_BOARD_DATA_INDEX_BME280_HUMIDITY = 3

THINGS_BOARD_DATA_INDEX_GPS_LATITUDE = 4
THINGS_BOARD_DATA_INDEX_GPS_LONGITUDE= 5
THINGS_BOARD_DATA_INDEX_GPS_ALTITUDE = 6

things_board_data_seq = [0] * len(THINGS_BOARD_DATA_NAME_SEQ)
# --------------------------------------------------------------------------
# GUI
TEXT_CAPTION = '_text_caption_'

TEXT_DATE_1 = '_text_date_1_'
TEXT_DATE_2 = '_text_date_2_'
TEXT_TIME_1 = '_text_time_1_'
TEXT_TIME_2 = '_text_time_2_'
TEXT_GPS = '_text_gps_'

TEXT_TEMPERATURE = '_text_temperature_'
TEXT_HUMIDITY = '_text_humidity_'
TEXT_PRESSURE = '_text_pressure_'
# --------------------------------------------------------------------------
# time
timezone_utc = pytz.utc
timezone_madrid = pytz.timezone('Europe/Madrid')
# --------------------------------------------------------------------------
