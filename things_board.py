# --------------------------------------------------------------------------
import json
import time
import paho.mqtt.client as mqtt
from global_var import *
# --------------------------------------------------------------------------
# Create the Window
def things_board_init():
    last_publishing_time_s[0] = 0
    things_board_client[0] = mqtt.Client()
    things_board_client[0].username_pw_set(THINGS_BOARD_DEVICE_ACCESS_TOKEN)
    things_board_client[0].connect(THINGS_BOARD_HOST, THINGS_BOARD_MQTT_PORT, THINGS_BOARD_CONNECTION_KEEP_ALIVE_INTERVAL_SECONDS)
    things_board_client[0].loop_start()
# --------------------------------------------------------------------------
def things_board_update_data(data_index,value):
    things_board_data_seq[data_index] = value
# --------------------------------------------------------------------------
def things_board_publish_data():
    now_s = time.time()
    elapsed_time_s = PUBLISHING_TIME_S if (last_publishing_time_s[0] == 0) else now_s - last_publishing_time_s[0]
    if (elapsed_time_s >= PUBLISHING_TIME_S) :
        things_board_data_seq[THINGS_BOARD_DATA_INDEX_TIME_STAMP] = now_s
        data_to_publish = dict(zip(THINGS_BOARD_DATA_NAME_SEQ, things_board_data_seq))
        things_board_client[0].publish(THINGS_BOARD_TELEMETRY_TOPIC, json.dumps(data_to_publish), 1)
        last_publishing_time_s[0] = now_s
# --------------------------------------------------------------------------
def things_board_close():
    things_board_client[0].loop_stop()
    things_board_client[0].disconnect()
# --------------------------------------------------------------------------