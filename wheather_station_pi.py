# --------------------------------------------------------------------------
# Simple GUI for a wheather station using a raspberry pi 4
# [hardware]
# [raspberry pi model 4B v1.1]
# Revision           : c03111
# SoC                : BCM2711
# RAM                : 4096Mb
# Storage            : MicroSD
# [GPS]
# u-blox 7 - GPS/GNSS reveiver. MakerHawk GPS Module 51 GT-U7
# [real time clock]
# RTC DS3231M
# [sesors: temperature, humidity, pressure]
# BME280
# --------------------------------------------------------------------------
from frame_date_time_gps import *
from frame_title import frame_title
from frame_sensor import *
from things_board import *
# --------------------------------------------------------------------------
#  GUI initialization
sg.theme('DarkAmber')  # Add a touch of color

# All the stuff inside your window.
layout = [frame_title
    , frame_time_date_1
    , frame_time_date_2
    , frame_sensors
          ]
main_form = sg.Window('maya weather station window'
                        , layout
                        , size=(1440,900)
                        , font=('Helvetica', '45')
                        , text_justification='c'
                        , auto_size_text=False)
# --------------------------------------------------------------------------
# Create the Window
def main():
    
    # init sensors
    sensors_init()

    # things board init
    things_board_init()

    # Event Loop to process "events" and get the "values" of the inputs
    while True:
        event, values = main_form.read(timeout=POLLING_TIME_MS)

        # update values
        data_time_gps_update(main_form)
        sensors_update(main_form)

        #publish data
        things_board_publish_data()

        if event == sg.WIN_CLOSED or event in ('Quit', 'Cancel'):  # if user closes window or clicks cancel
            break

    # things board init
    things_board_close()

    # close the window
    main_form.close()


# --------------------------------------------------------------------------
if __name__ == '__main__':
    main()
# --------------------------------------------------------------------------
