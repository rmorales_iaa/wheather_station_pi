# [real time clock]
# RTC DS3231M
# info: http://www.intellamech.com/RaspberryPi-projects/rpi_RTCds3231
# [gps]
# u-blox 7 - GPS/GNSS reveiver. MakerHawk GPS Module 51 GT-U7
# datasheet #----------------------
# --------------------------------------------------------------------------
import PySimpleGUI as sg
import datetime
from things_board import *
from global_var import *
# --------------------------------------------------------------------------

# frame layout
layout_time_date_1 = [[sg.Text('DATE 1 NOT INITIALIZED'
                               , key=TEXT_DATE_1
                               , font=('Monospace', '60'))]
    , [sg.Text('DATE 1 NOT INITIALIZED'
               , key=TEXT_TIME_1
               , font=('Monospace', '50'))]
                      ]

layout_time_date_2 = [[sg.Text('DATE 2 NOT INITIALIZED'
                               , key=TEXT_DATE_2
                               , background_color='black'
                               , text_color='#929292'
                               , font=('Helvetica', '40'))]
    , [sg.Text('TIME 2 NOT INITIALIZED'
               , key=TEXT_TIME_2
               , background_color='black'
               , text_color='#929292'
               , font=('Helvetica', '40'))]
    , [sg.Text('GPS INFO NO INITIALIZED'
               , key=TEXT_GPS
               , background_color='black'
               , text_color='#929292'
               , font=('Helvetica', '37'))]
                      ]
# frame definition
frame_time_date_1 = [sg.Frame(title=''
                              , layout=layout_time_date_1
                              , element_justification='left')
                     ]

frame_time_date_2 = [sg.Frame(title=''
                              , layout=layout_time_date_2
                              , background_color='Black'
                              , element_justification='left')
                     ]


# --------------------------------------------------------------------------
def data_time_gps_update(form):
    now_utc = datetime.datetime.now(tz=timezone_utc)
    now_madrid = now_utc.astimezone(timezone_madrid)
    current_date_1 = now_utc.strftime('%Y-%m-%d')
    current_date_2 = now_utc.strftime('%d %B,%A,%Y')
    current_time_1 = now_utc.strftime('%T %Z')
    current_time_2 = now_madrid.strftime('(%T %Z ') + timezone_madrid.zone + ')'
    form[TEXT_DATE_1].Update(current_date_1)
    form[TEXT_DATE_2].Update(current_date_2)
    form[TEXT_TIME_1].Update(current_time_1)
    form[TEXT_TIME_2].Update(current_time_2)

    gps_info(form)


# --------------------------------------------------------------------------
# https://pypi.org/project/gpsdshm/
def gps_info(form):
    online = GPSD_SHM.status
    gps_info = 'GPS is offline'
    last_value_latitude = 'None'
    last_value_longitude = 'None'
    last_value_altitude = 'None'

    if online:
        last_value_latitude = 'lat: ' + "{:.6f}".format(GPSD_SHM.fix.latitude)
        last_value_longitude = 'lon: ' + "{:.6f}".format(GPSD_SHM.fix.longitude)
        last_value_altitude = 'alt: ' + "{:.6f}".format(GPSD_SHM.fix.altitude)
        gps_info = last_value_latitude + "\t" + last_value_longitude + "\t" + last_value_altitude

    form[TEXT_GPS].Update(gps_info)

# --------------------------------------------------------------------------
